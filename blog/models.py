from django.db import models

# Create your Blog models here.
#title
#pub_date
#body
#image

class Blog(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(default='')
    image = models.ImageField(upload_to='images/')
    pub_date = models.DateTimeField()

    def __str__(self):
        return self.title[0:20]

    def summary(self):
        return self.body[:100] + '...'

    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y')



#Add the Blog app to the settings
#create a migration
#migrate
# add to the admin
